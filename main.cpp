#include <iostream>
#include <time.h>
#include <Windows.h>
#define TIME clock()

typedef void (*func)();

int height = 1080;
int width = 1920;

// side length of cell squares in pixels
int scale = 8;

int framerate = 100;
const int cooldown = 60000; // cooldown for reset
int timer = 0;
int delay;

int n_states = 1;
char* board0;
char* board1;
int last_frametime = 0;
int gametype = 1;


HWND wallpaper_hwnd = nullptr;
HDC hDC_Desktop;

static HDC memoryDC;     //back buffer

static BITMAP bitmap;

HBITMAP hBitmap;

//brushes
HBRUSH* BRUSHES;
RECT* r = new RECT;

// functions
int initialize();
void render();
func update;

//gamemodes
void conway();
void sandpile();
void spread();
void cyclic();
void brain();
void cyclic2();
void r_sandpile();
void spread2();

//utlities
double QqhToRgb(double q1, double q2, double hue);
COLORREF HlsToRgb(double h, double l, double s);
BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);
void get_wallpaper_window();
int coords(int x, int y);
void randomize();

int main(int argc, char* argv[])
{
	srand(TIME);
	std::cout << "choose gametype: ";
	std::cin >> gametype;

	switch (gametype)
	{

	case 0:
		n_states = 2;
		update = conway;
		framerate = 16;
		break;

	case 1:
		n_states = 4;
		update = sandpile;
		framerate = 100;
		break;

	case 2:
		scale = 2;
		n_states = 2;
		update = spread;
		framerate = 10;
		break;

	case 3:
		n_states = 20;
		update = cyclic;
		framerate = 30;
		break;

	case 4:
		n_states = 3;
		framerate = 16;
		update = brain;
		break;

	case 5:
		n_states = 12;
		update = cyclic2;
		framerate = 30;
		break;

	case 6:
		scale = 16;
		n_states = 4;
		update = r_sandpile;
		framerate = 100;
		break;

	case 7:
		scale = 2;
		n_states = 2;
		update = spread2;
		framerate = 10;
		break;

	default:
		n_states = 2;
		update = conway;
		framerate = 16;
		break;
	}

	if (initialize() == 1)
	{
		return 1;
	}

	switch (gametype)
	{
	case 0:
		randomize();
		BRUSHES[0] = CreateSolidBrush(RGB(20, 20, 20));
		BRUSHES[1] = CreateSolidBrush(RGB(0, 120, 0));
		break;

	case 1:

		BRUSHES[0] = CreateSolidBrush(RGB(34, 34, 34));
		BRUSHES[1] = CreateSolidBrush(RGB(0, 77, 51));
		BRUSHES[2] = CreateSolidBrush(RGB(0, 130, 0));
		BRUSHES[3] = CreateSolidBrush(RGB(113, 170, 0));
		break;

	case 2:
		BRUSHES[0] = CreateSolidBrush(RGB(20, 20, 20));
		BRUSHES[1] = CreateSolidBrush(RGB(0, 120, 0));
		board1[coords(width / scale / 2, height / scale / 2)] = 1;
		break;

	case 3:
		randomize();
		for (int i = 0; i < n_states; i++){BRUSHES[i] = CreateSolidBrush(HlsToRgb(360 *i / n_states , 0.50, 0.50));}
		break;

	case 4:
		BRUSHES[0] = CreateSolidBrush(RGB(20, 20, 20));
		BRUSHES[1] = CreateSolidBrush(RGB(0, 77, 0));
		BRUSHES[2] = CreateSolidBrush(RGB(0, 120, 0));
		randomize();
		break;

	case 5:
		randomize();
		for (int i = 0; i < n_states; i++) { BRUSHES[i] = CreateSolidBrush(HlsToRgb(360 * i / n_states, 0.50, 0.50)); }
		break;

	case 6:
		BRUSHES[0] = CreateSolidBrush(RGB(34, 34, 34));
		BRUSHES[1] = CreateSolidBrush(RGB(0, 77, 51));
		BRUSHES[2] = CreateSolidBrush(RGB(0, 130, 0));
		BRUSHES[3] = CreateSolidBrush(RGB(113, 170, 0));
		srand(TIME);
		break;

	case 7:
		BRUSHES[0] = CreateSolidBrush(RGB(20, 20, 20));
		BRUSHES[1] = CreateSolidBrush(RGB(0, 120, 0));
		board1[coords(width / scale / 2, height / scale / 2)] = 1;
		break;

	default:

		randomize();
		BRUSHES[0] = CreateSolidBrush(RGB(20, 20, 20));
		BRUSHES[1] = CreateSolidBrush(RGB(0, 120, 0));
	}

	//for (int i = 0; i < 40 000 000; i++) { update(); }


	while (true)
	{

		update();
		render();

		// printing theoretical max framerate
		// std::cout << "fps: " << 1000.0 / (clock() - last_frametime) << "\n";

		// keeping framerate
		delay = CLOCKS_PER_SEC / framerate + last_frametime;
		std::cout << "delay: " << delay << " before ";
		while (delay > TIME) {}
		std::cout << (last_frametime = TIME) << std::endl;

	}
}


int initialize()
{
	// dektop 
	get_wallpaper_window();
	hDC_Desktop = GetDC(wallpaper_hwnd);


	HMONITOR mon = MonitorFromWindow(wallpaper_hwnd, 0);


	MONITORINFOEX monitorInfoEx;
	monitorInfoEx.cbSize = sizeof(monitorInfoEx);
	GetMonitorInfo(mon, &monitorInfoEx);

	// Get the physical width and height of the monitor
	DEVMODE devMode;
	devMode.dmSize = sizeof(devMode);
	devMode.dmDriverExtra = 0;
	EnumDisplaySettings(monitorInfoEx.szDevice, ENUM_CURRENT_SETTINGS, &devMode);
	width = devMode.dmPelsWidth;
	height = devMode.dmPelsHeight;

	std::srand(TIME);


	// initializing the board 
	board0 = new char[width * height / (scale * scale)];
	board1 = new char[width * height / (scale * scale)];

	for (int i = 0; i < width * height / (scale * scale); i++) 
	{
		board0[i] = board1[i] = 0;
	}

	// hDC_Desktop = GetDC(0);

	BRUSHES = new HBRUSH[n_states];

	//create memory device (back buffer)

	hBitmap = CreateCompatibleBitmap(hDC_Desktop, width, height);

	memoryDC = CreateCompatibleDC(hDC_Desktop);

	SelectObject(memoryDC, hBitmap);

	DeleteObject(hBitmap);

	// std::cout << "ticks per sec: " << CLOCKS_PER_SEC << "\ndone initializing\n";

	return 0;
}


void render()
{
	r->top = 0;
	r->bottom = height;
	r->left = 0;
	r->right = width;
	FillRect(memoryDC, r, BRUSHES[0]);

	// draw pixels

	for (int x = 0; x < width / scale; x++)
	{
		for (int y = 0; y < height / scale; y++)
		{
			if (board1[coords(x,y)])
			{
				r->top = y * scale;
				r->bottom = (y + 1) * scale;
				r->left = x * scale;
				r->right = (x + 1) * scale;
				FillRect(memoryDC, r, BRUSHES[ board1[coords(x, y)] ]);
			}
		}
	}

	BitBlt(hDC_Desktop, 0, 0, width, height, memoryDC, 0, 0, SRCCOPY);
	RedrawWindow(wallpaper_hwnd, NULL, NULL, RDW_NOERASE | RDW_INVALIDATE | RDW_UPDATENOW);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void conway()
{
	if (timer > TIME)
	{
		int neighbors;

		for (int x = 0; x < width / scale; x++)
		{
			for (int y = 0; y < height / scale; y++)
			{
				// get the number of neighbors
				neighbors = 0 - board0[coords(x,y)];

				for (int i = -1; i < 2; i++)
				{
					for (int j = -1; j < 2; j++)
					{
						neighbors += board0[coords((x + i + width / scale) % (width / scale),(y + j + height / scale) % (height / scale))];
					}
				}

				// see whether the cell survives
				if (neighbors < 2 || neighbors > 3)
				{
					board1[coords(x, y)] = 0;
				}

				else if (neighbors == 3)
				{
					board1[coords(x, y)] = 1;
				}

				else
				{
					board1[coords(x, y)] = board0[coords(x, y)];
				}

			}

		}


		// replace current state with future state as the fututre is NOW
		char* temp = board1;
		board1 = board0;
		board0 = temp;
	}

	else // randomize board
	{
		randomize();

		timer = TIME + cooldown;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void pile(int posx, int posy)
{
	if (posy > height / scale || posx > width / scale || posx < 0 || posy < 0) return;

	if (++board1[coords(posx, posy)] < 4) return;

	board1[coords(posx, posy)] -= 4;

	pile(posx, posy + 1);
	pile(posx + 1, posy);
	pile(posx - 1, posy);
	pile(posx, posy - 1);
	return;
}


void sandpile()
{
	pile((width / scale) / 2 - 2, (height / scale) / 2);
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spread()
{
	int neighbors;

	for (int x = 0; x < width / scale; x++)
	{
		for (int y = 0; y < height / scale; y++)
		{
			// get the number of neighbors
			neighbors = 0 - board1[coords(x, y)];

			for (int i = -1; i < 2; i++)
			{
				neighbors += board1[coords(max((min(x + i, width / scale - 1)), 0), (y + height / scale) % (height / scale))];
				neighbors += board1[coords((x + width / scale) % (width / scale), max((min(y + i, height / scale - 1)), 0))];
			}

			// see whether the cell survives
			if (neighbors == 1 || neighbors == 3)
			{
				board0[coords(x,y)] = 1;
			}

		}

	}

	char* temp = board1;
	for (int i = 0; i < width * height / (scale * scale); i++)
	{
		board1[i] = board0[i];
	}

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void cyclic()
{
	int cur;
	int target;
	int next;

	for (int r = 0; r < width / scale; r++)
	{
		for (int c = 0; c < height / scale; c++)
		{
			cur = board1[coords(r, c)];
			target = (cur + 1) % n_states;
			next = cur;

			// Check the surrounding 8 cells
			for (int dr = -1; dr <= 1; dr++)
			{
				for (int dc = -1; dc <= 1; dc++)
				{
					if (dr == 0 && dc == 0) continue;

					if (board1[coords((r + dr + width / scale) % (width / scale), (c + dc + height / scale) % (height / scale))] == target)
					{
						next = target;
					}
				}
			}

			board0[coords(r, c)] = next;
		}
	}

	// replace current state with future state as the fututre is NOW

	char* temp = board1;
	board1 = board0;
	board0 = temp;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void brain()
{
	int neighbors;

	for (int x = 0; x < width / scale; x++)
	{
		for (int y = 0; y < height / scale; y++)
		{
			// get the number of neighbors
			neighbors = 0 - (board1[coords(x, y)] == 2);

			for (int i = -1; i < 2; i++)
			{
				for (int j = -1; j < 2; j++)
				{
					neighbors += (board1[coords((x + i + width / scale) % (width / scale), (y + j + height / scale) % (height / scale))] == 2);
				}
			}

			// see whether the cell survives
			if (neighbors == 2 && board1[coords(x, y)] == 0)
			{
				board0[coords(x, y)] = 2;
			}

			else if (board1[coords(x, y)] == 2)
			{
				board0[coords(x, y)] = 1;
			}

			else
			{
				board0[coords(x, y)] = max(0, board1[coords(x, y)] - 1);
			}

		}

	}

	// replace current state with future state as the fututre is NOW
	char* temp = board1;
	board1 = board0;
	board0 = temp;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void cyclic2()
{
	int cur;
	int target;
	int next;

	for (int r = 0; r < width / scale; r++)
	{
		for (int c = 0; c < height / scale; c++)
		{
			cur = board1[coords(r, c)];
			target = (cur + 1) % n_states;
			next = cur;

			// Check the surrounding 8 cells
			for (int dr = -1; dr <= 1; dr++)
			{
				if (dr == 0) continue;

				if (board1[coords((r + dr + width / scale) % (width / scale), c )] == target)
				{
					next = target;
				}
			}

			for (int dc = -1; dc <= 1; dc++)
			{
					if (dc == 0) continue;

					if (board1[coords(r, (c + dc + height / scale) % (height / scale))] == target)
					{
						next = target;
					}
			}

			board0[coords(r, c)] = next;
		}
	}

	// replace current state with future state as the fututre is NOW

	char* temp = board1;
	board1 = board0;
	board0 = temp;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void r_sandpile()
{
	pile(rand() % (width / scale), rand() % (height / scale));
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void spread2()
{
	int neighbors;

	for (int x = 0; x < width / scale; x++)
	{
		for (int y = 0; y < height / scale; y++)
		{
			// get the number of neighbors
			neighbors = 0 - board1[coords(x, y)];

			for (int i = -1; i < 2; i++)
			{
				for (int j = -1; j < 2; j++)
				{
					neighbors += board1[coords(max((min(x + i, width / scale - 1)), 0), max((min(y + j, width / scale - 1)), 0))];
				}
			}

			// see whether the cell survives
			if (neighbors == 1 || neighbors == 3)
			{
				board0[coords(x, y)] = 1;
			}

		}

	}
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////

double QqhToRgb(double q1, double q2, double hue)
{
	if (hue > 360) hue -= 360;
	else if (hue < 0) hue += 360;

	if (hue < 60) return q1 + (q2 - q1) * hue / 60;
	if (hue < 180) return q2;
	if (hue < 240) return q1 + (q2 - q1) * (240 - hue) / 60;
	return q1;
}

COLORREF HlsToRgb(double h, double l, double s)
{
	double p2;
	if (l <= 0.5) p2 = l * (1 + s);
	else p2 = l + s - l * s;

	double p1 = 2 * l - p2;
	double double_r, double_g, double_b;
	if (s == 0)
	{
		double_r = l;
		double_g = l;
		double_b = l;
	}
	else
	{
		double_r = QqhToRgb(p1, p2, h + 120);
		double_g = QqhToRgb(p1, p2, h);
		double_b = QqhToRgb(p1, p2, h - 120);
	}

	// Convert RGB to the 0 to 255 range.

	return RGB(double_r * 255.0, double_g * 255.0, double_b * 255.0);
}


BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam)
{
	HWND p = FindWindowEx(hwnd, NULL, L"SHELLDLL_DefView", NULL);
	if (p)
	{
		wallpaper_hwnd = FindWindowEx(NULL, hwnd, L"WorkerW", NULL);
	}
	return true;

}

void get_wallpaper_window()
{
	HWND progman = FindWindow(L"ProgMan", NULL);
	SendMessageTimeout(progman, 0x052C, 0, 0, SMTO_NORMAL, 1000, nullptr);

	EnumWindows(EnumWindowsProc, NULL);
	return;
}

inline int coords(int x, int y) 
{
	return x * (height / scale) + y;
}

void randomize()
{
	srand(TIME);

	for (int i = 0; i < width * height / (scale * scale); i++) 
	{ 
		board0[i] = rand() % n_states;
		board1[i] = rand() % n_states; 
	}
}